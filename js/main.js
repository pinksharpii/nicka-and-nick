(function ($) {
	$(document).ready(function(){
		function isValidEmailAddress(emailAddress) {
		    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		    return pattern.test(emailAddress);
		}

		function isValidPhone(phone) {
			var pattern = /[0-9\-\(\)\s]+/;
		    return pattern.test(phone);
		}

		var $form = $('form');

		$form.on('submit', function(e){
			$('.error').remove();

			$('input').each(function(){
				if(!$(this).val().trim() && $(this).prop('required'))
				{
					$('<p class="error">This field is required.</p>').insertAfter($(this));
				}
				else if($(this).attr('id') ==  'email' && !isValidEmailAddress($(this).val()))
				{
					$('<p class="error">Please provide a valid email address.</p>').insertAfter($(this));
				}
				else if($(this).attr('id') ==  'phone' && ($(this).val().trim() && !isValidPhone($(this).val())))
				{
					$('<p class="error">Please provide a valid phone number.</p>').insertAfter($(this));
				}
			});

			e.preventDefault();
			if(!$('.error').length)
			{
				$.ajax({
					data: $form.serialize(),
					method: 'post',
					url: '/process-form.php',
					success: function(data){
						$form.find('input[type="text"]').val('');
						$('<p>We have receieved your request. Thank you! We will be in touch.</p>').appendTo($form);
					}
				});
			}

		});
	});
}(jQuery));